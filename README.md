## Reverse Proxy cluster test

This repository contains the following application:

- Traefik + Let's Encrypt or LE (TLS provider) using TLS-ALPN-01 as TLS Challenge;

This was created to act as a test reverse-proxy for my undergraduate thesis.

## Title (english): Use of tools from Infrastructure as Code and a Linux Container Orchestrator to automate and facilitate the deployment of software in a production environment in an IaaS architecture

The aim of this paper is to present a way of creating a secure, efficient, fault-tolerant and initially low-cost production cluster, using the container orchestrator Docker Swarm and some Infrastructure as Code tools, such as Ansible and Terraform.

**Title (portuguese)**: Uso de ferramentas de Infraestrutura como Código e um Orquestrador de Linux contêineres para automatizar e facilitar a implantação de softwares em ambiente de produção em uma arquitetura IaaS

Author: [Mateus Villar](https://mateusvillar.com)

Graduation: [Computer Science](https://unifei.edu.br/prg/cursos-presenciais/itajuba-campus-sede/ciencia-da-computacao/)

Universidade Federal de Itajubá - [UNIFEI](https://unifei.edu.br/)

## All Bachelor's Thesis Repositories

- [pirasbro/bachelors-thesis/main-code](https://gitlab.com/pirasbro/bachelors-thesis/main-code)
- [pirasbro/bachelors-thesis/reverse-proxy](https://gitlab.com/pirasbro/bachelors-thesis/reverse-proxy)
- [pirasbro/bachelors-thesis/webserver](https://gitlab.com/pirasbro/bachelors-thesis/webserver)

## Deployment

### Automated

Deploy with docker swarm.

Create an environment variable with the password. Use openssl to generate the "hashed" version of the password and store it in an environment variable.

We don't want to put the password in an environment variable, so you should use htpasswd to generate an unique encrypted. To create user:password pair, it's possible to use this command:

```bash
echo $(htpasswd -nb "<your-user-here>" "<your-password-here>") | sed -e s/\\$/\\$\\$/g
```

You received a similar output:

```output
<your-user-here>:$$apr1$$qslLHHCO$$aBMSJz.wqqFST9E6j1ExJ0
```

Replace that inside `docker-stack.traefik.yml` in the label below:

```yml
...
services:
  traefik:
    ...
    deploy:
      placement:
        ....
      labels:
        - traefik.http.middlewares.admin-auth.basicauth.users=<your-user-here>:$$apr1$$qslLHHCO$$aBMSJz.wqqFST9E6j1ExJ0
```

Finally, start Traefik stack:

```
make deploy
```

This will create a label in this node, so that Traefik is always deployed to the same node and uses the same volume. It also creates a public network that will be shared with Traefik and the containers that should be accessible from the outside.

**It's done!**

### Manually

Create a label in this node, so that Traefik is always deployed to the same node and uses the same volume:

```
docker node update --label-add traefik-public.traefik-public-certificates=true <node-name-here>
```

or

```
make label
```

Create a public network that will be shared with Traefik and the containers that should be accessible from the outside:

```
docker network create --driver=overlay traefik-public
```

or

```
make stack-network
```

Create an environment variable with the password. Use openssl to generate the "hashed" version of the password and store it in an environment variable.

We don't want to put the password in an environment variable, so you should use htpasswd to generate an unique encrypted. To create user:password pair, it's possible to use this command:

```bash
echo $(htpasswd -nb "<your-user-here>" "<your-password-here>") | sed -e s/\\$/\\$\\$/g
```

You received a similar output:

```output
<your-user-here>:$$apr1$$qslLHHCO$$aBMSJz.wqqFST9E6j1ExJ0
```

Replace that inside `docker-stack.traefik.yml` in the label below:

```yml
...
services:
  traefik:
    ...
    deploy:
      placement:
        ....
      labels:
        - traefik.http.middlewares.admin-auth.basicauth.users=<your-user-here>:$$apr1$$qslLHHCO$$aBMSJz.wqqFST9E6j1ExJ0
```

Finally, start Traefik stack:

```
docker stack deploy -c <(docker-compose -f docker-stack.traefik.yml config) reverse-proxy
```

or

```
make stack
```

**It's done!**

##### Helpful links:

- Breaking changes from Traefik v2.x to v2.2.2: https://docs.traefik.io/migration/v2/
- Some steps were copied directly by: https://dockerswarm.rocks/traefik/
- More information on Traefik setup: https://dockerswarm.rocks/traefik/
- More information on Docker Expertimental Features: https://docs.docker.com/config/daemon/prometheus/ and https://thenewstack.io/how-to-enable-docker-experimental-features-and-encrypt-your-login-credentials/
- More information on Challenge Types: https://letsencrypt.org/docs/challenge-types/
- Redirect non-www and vice-versa: https://community.traefik.io/t/global-redirect-www-to-non-www-with-https-redirection/2313/26

## TODO:

- gotta learn about secure headers used!!!
- add encrypted file for passwords using secrets;