# Get Makefile full path
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
# Get Makefile dir name only
root_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

# Force to use buildkit for all images and for docker-compose to invoke
# Docker via CLI (otherwise buildkit isn't used for those images)
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

# HELP
# This will output the help for each task
.PHONY: help -

# Automated helper
help: ## Show this help information.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

-: ## 

# DEPLOY - docker-stack commands

# Get node hostname
node_hostname := $(shell docker node inspect self --format='{{.Description.Hostname}}')
label: ## Create a label in this node, so that Traefik is always deployed to the same node and uses the same volume.
	@docker node update --label-add traefik-public.traefik-public-certificates=true $(node_hostname)

stack-network: ## Create Traefik Public network, if it does not exist already.
	@docker network inspect traefik-public >/dev/null 2>&1 || \
		docker network create --driver overlay traefik-public

stack: SHELL:=/bin/bash
stack: ## Deploy/update project using docker Stack.
	@docker stack deploy \
		--compose-file \
			<(docker-compose \
				--file docker-stack.traefik.yml config) \
		$(root_dir)

stack-down: ## Remove docker Traefik stack.
	@docker stack rm $(root_dir)

deploy:	label stack-network stack ## Create label, network and deploy the stack.